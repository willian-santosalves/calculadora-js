import assert from 'assert'
import chai from 'chai'
import Calculadora from '../src/Calculadora.js'

const expect = chai.expect;

describe('Teste de Soma', () => {
    it('Deve somar 4 e 5 resultando em 9', () => {
        let resultado = Calculadora.soma(4, 5);
        expect(resultado).to.be.eq(9).and.to.be.a('number');
    });

    it('Verifica se o resultado é maior que 8 ', () => {
        let resultado = Calculadora.soma(4, 5);
        expect(resultado).is.above(8);
    })

    it('Verifica se o resultado é menor que 15', () => {
        let resultado = Calculadora.soma(4, 5);
        expect(resultado).is.below(15)
        
    })
});

describe('Teste de Subtração', () => {
    it('Deve subtrair 5 e 4 resultando em 1', () => {
        let resultado = Calculadora.subtracao(5, 4);
        expect(resultado).to.be.eq(1);
    });

    it('Deve subtrair 5 de 4 resultando em -1', () => {
        let resultado = Calculadora.subtracao(4, 5);
        expect(resultado).to.not.equal(0)
    })
});

describe('Teste de Multiplicação', () => {
    it('Deve multiplicar 4 e 5 resultando em 20', () => {
        let resultado = Calculadora.multiplicacao(4, 5);
        expect(resultado).to.be.eq(20);
    });

    it.only('Deve multiplicar 100 e 100 resultando em 1000', () => {
        let resultado = Calculadora.multiplicacao(100, 100)
        expect(resultado).to.be.eq(10000).and.to.be.a('number')
    })
});

describe('Teste de Divisão', () => {
    it('Deve somar 20 e 5 resultando em 4', () => {
        let resultado = Calculadora.divisao(20, 5);
        expect(resultado).to.be.eq(4);
    });
});