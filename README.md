# Calculadora com Javascript utilizando **Mocha** e **Chai**

---

# Indice

- [Sobre](#-sobre)
- [Requisitos](#-requisitos)
- [Como baixar o projeto](#-como-baixar-o-projeto)
- [Rodando os Testes](#-rodando-os-testes)
- [Construído com](#-construido-com)
- [Referências](#-referencias-de-readme)

---

## Sobre

É um projeto de uma Calculadora em Javascript, sugerido no Programa de Bolsas, para praticar Testes com as ferramentas **Mocha** e **Chai**.

---

## Requsitos

Você precisará ter essas ferramentas instaladas um sua máquina.

- [Visual Studio Code](https://code.visualstudio.com/)
- [Node.js](https://nodejs.org/en)
- [Mocha](https://mochajs.org/)
- [Chai](https://www.chaijs.com/)

---

## Como baixar o projeto

```bash
    #Abrir o Terminal

    # Clonar o repositório
    $ git clone https://gitlab.com/willian-santosalves/calculadora-js.git

    # Entrar no diretório
    $ cd calculadora-js

    #Iniciar o npm e diretório node_modules
    $ npm init

    #Instalar as ferramentas Mocha e Chai se necessário
    $ npm i -D mocha chai
```

---

## Rodando os Testes

```bash
    #Para rodar o script dos testes
    $ npm test
```

--- 

## Construído com

- [Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)

---

## Referências de README

- [Meu Próprio Projeto no GitHub](https://github.com/willian-santosalves/alem-de-marte)
- [lohhans/README-PTBR.md](https://gist.github.com/lohhans/f8da0b147550df3f96914d3797e9fb89)