export default class Calculadora {

    static soma(a, b) {
        return a + b;
    }

    static subtracao(a, b) {
        return a - b;
    }

    static multiplicacao(a, b) {
        return a * b;
    }

    static divisao(a, b) {
        return a / b;
    }

    static potencia(a, b) {
        return Math.pow(a, b)
    }
}